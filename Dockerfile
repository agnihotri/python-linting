FROM python:3.10-alpine
WORKDIR /app
COPY requirements.txt /app

RUN apk update && apk add build-base libffi-dev && \
    apk --no-cache add --virtual .build build-base && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --no-cache del .build
