# Python Linting

A Python linting Docker image to be used by the gitlab pipeline [gitlab-ci-pipeline](https://gitlab.com/mafda/gitlab-ci-pipeline).

--- 

Made with 💙 by [agnihotri](https://agnihotri7.github.io/).
